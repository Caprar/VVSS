package Controller;

import Domain.Carte;
import Service.IClient;
import Service.IServer;
import Service.LicentaException;

import java.util.List;

/**
 * Created by User on 19-May-18.
 */

public class ControllerClient implements IClient {
    // atributele private ale clasei
    private IServer serv;
    private Carte carte;

    // constructorul clasei
    public ControllerClient(IServer s) {
        this.serv = s;
    }

    // CARTE

    // functia de returnare a unei entitatii
    // care are id-ul dat
    public Carte getCarte(String id) {
        try {
            return serv.getCarte(id);
        } catch (LicentaException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Carte> getAll(){
        try {
            return serv.getAll();
        } catch (LicentaException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Carte getAdaugare(Carte carte){
        try{
            return serv.getAdaugare(carte);
        } catch (LicentaException e) {
            e.printStackTrace();
        }
        return new Carte();
    }
}

