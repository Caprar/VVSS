package Domain;

import java.io.Serializable;

/**
 * Created by User on 19-May-18.
 */


public class Carte implements HasId<String>,Serializable {
    private String titlu;
    private String referenti;
    private String anAparitie;
    private String cuvinteCheie;

    public Carte(){
        titlu = "";
        referenti = "";
        anAparitie = "";
        cuvinteCheie = "";
    }

    public Carte(String titlu, String referenti, String anAparitie, String cuvinteCheie){
        this.titlu = titlu;
        this.referenti = referenti;
        this.anAparitie = anAparitie;
        this.cuvinteCheie = cuvinteCheie;
    }

    public String getId() {
        return titlu;
    }

    public void setId(String titlu) {
        this.titlu = titlu;
    }

    public String getReferenti() {
        return referenti;
    }

    public void setReferenti(String ref) {
        this.referenti = ref;
    }

    public String getAnAparitie() {
        return anAparitie;
    }

    public void setAnAparitie(String anAparitie) {
        this.anAparitie = anAparitie;
    }

    public String getCuvinteCheie() {
        return cuvinteCheie;
    }

    public void setCuvinteCheie(String cuvinteCheie) {
        this.cuvinteCheie = cuvinteCheie;
    }

    @Override
    public String toString(){
        return this.titlu+";"+this.referenti+";"+this.anAparitie+";"+this.cuvinteCheie;
    }
}
