package Domain;

/**
 * Created by User on 19-May-18.
 */

public interface HasId <ID>{
    ID getId();
    void setId(ID id);
}
