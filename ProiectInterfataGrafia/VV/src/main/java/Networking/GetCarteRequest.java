package Networking;

/**
 * Created by User on 19-May-18.
 */

public class GetCarteRequest implements Request {
    private String idCarte;

    public GetCarteRequest(String id) {
        this.idCarte = id;

    }

    public String getIdCarte() {
        return this.idCarte;
    }
}
