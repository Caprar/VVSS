package Networking;

import Service.IServer;

import java.net.Socket;

/**
 * Created by User on 19-May-18.
 */

public class ObjectConcurrentServer extends AbsConcurrentServer{
    // atributele private ale clasei
    private IServer licentaServer;

    // constructorul clasei
    public ObjectConcurrentServer(int port, IServer lServer) {
        super(port);
        this.licentaServer = lServer;
        System.out.println("Licenta- ObjectConcurrentServer");
    }

    // functia de creare a worker-ului
    @Override
    protected Thread createWorker(Socket client) {
        ClientObjectWorker worker = new ClientObjectWorker(licentaServer, client);
        Thread tw=new Thread(worker);
        return tw;
    }
}
