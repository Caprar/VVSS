package Networking;

import Domain.Carte;

/**
 * Created by User on 19-May-18.
 */

public class GetCarteResponse implements Response {
    private Carte carte;

    public GetCarteResponse(Carte carte){
        this.carte = carte;
    }

    public Carte getCarte(){
        return this.carte;
    }
}


