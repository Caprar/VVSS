package Networking;

import Domain.Carte;
import Service.IClient;
import Service.IServer;
import Service.LicentaException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by User on 19-May-18.
 */

public class ServerObjectProxy implements IServer {
    // atributele private ale clasei
    private String host;
    private int port;
    private IClient client;
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private Socket connection;
    private BlockingQueue<Response> qresponses;
    private volatile boolean finished;

    // constructorul clasei
    public ServerObjectProxy(String host, int port) {
        this.host = host;
        this.port = port;
        qresponses = new LinkedBlockingQueue<Response>();
    }

    public Carte getCarte(String id) throws LicentaException {
        initializeConnection();
        Response response = null;

        try {
            sendRequest(new GetCarteRequest(id));
            response = readResponse();
        } catch (LicentaException e) {
            e.printStackTrace();
        }

        if (response instanceof GetCarteResponse) {
            GetCarteResponse r = (GetCarteResponse) response;
            return r.getCarte();
        }

        if (response instanceof ErrorResponse) {
            ErrorResponse err = (ErrorResponse) response;
            closeConnection();
            throw new LicentaException(err.getMessage());
        }
        return null;
    }

    public List<Carte> getAll() throws LicentaException {
        Response response = null;

        try {
            sendRequest(new GetAllRequest());
            response = readResponse();
        } catch (LicentaException e) {
            e.printStackTrace();
        }

        if (response instanceof GetAllResponse) {
            GetAllResponse r = (GetAllResponse) response;
            return r.getCartes();
        }

        if (response instanceof ErrorResponse) {
            ErrorResponse err = (ErrorResponse) response;
            closeConnection();
            throw new LicentaException(err.getMessage());
        }

        return null;
    }


    // functia care salveaza o entitatea
    public Carte getAdaugare(Carte carte) throws LicentaException {
        Response response = null;
        try {
            sendRequest(new AdaugareCarteRequest(carte));
            response = readResponse();
        } catch (LicentaException e) {
            e.printStackTrace();
        }

        if (response instanceof AdaugareCarteResponse) {
            AdaugareCarteResponse r = (AdaugareCarteResponse) response;
            return r.getCarte();
        }

        if (response instanceof ErrorResponse) {
            ErrorResponse err = (ErrorResponse) response;
            closeConnection();
            throw new LicentaException(err.getMessage());
        }

        return null;
    }

    // functia de inchidere a conexiunii
    private void closeConnection() {
        finished = true;

        try {
            input.close();
            output.close();
            connection.close();
            client = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // functia de trimitere a cererii
    private void sendRequest(Request request) throws LicentaException {
        try {
            output.writeObject(request);
            output.flush();
        } catch (IOException e) {
            throw new LicentaException("Error sending object " + e);
        }
    }

    // functia de citire a raspunsului
    private Response readResponse() throws LicentaException {
        Response response = null;
        try {
            response = qresponses.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    // functia de initializare a conexiunii
    private void initializeConnection() throws LicentaException {
        try {
            connection = new Socket(host, port);
            output = new ObjectOutputStream(connection.getOutputStream());
            output.flush();
            input = new ObjectInputStream(connection.getInputStream());
            finished = false;
            startReader();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // functia de start a reader-ului
    private void startReader() {
        Thread tw = new Thread(new ReaderThread());
        tw.start();
    }

    // functia reader-ului thread
    private class ReaderThread implements Runnable {
        public void run() {
            while (!finished) {
                try {
                    Object response = input.readObject();
                    System.out.println("response received " + response);
                    if (response instanceof UpdateResponse) {
                    }
                    else {
                        try {
                            qresponses.put((Response) response);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IOException e) {
                    System.out.println("Reading error " + e);
                } catch (ClassNotFoundException e) {
                    System.out.println("Reading error " + e);
                }
            }
        }
    }
}
