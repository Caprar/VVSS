package Networking;

import java.net.Socket;

/**
 * Created by User on 19-May-18.
 */

public abstract class AbsConcurrentServer extends AbstractServer{
    // atributele private ale clasei
    protected abstract Thread createWorker(Socket client) ;

    // constructorul clasei
    public AbsConcurrentServer(int port) {
        super(port);
        System.out.println("Concurrent AbstractServer");
    }

    // functia de proces a cererii
    protected void processRequest(Socket client) {
        Thread tw=createWorker(client);
        tw.start();
    }
}
