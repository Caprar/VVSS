package Networking;

/**
 * Created by User on 19-May-18.
 */

public class ErrorResponse implements Response {
    private String message;
    public ErrorResponse(String message) {
        this.message = message;
    }
    public String getMessage() { return message; }
}

