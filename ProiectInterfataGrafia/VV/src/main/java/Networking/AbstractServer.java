package Networking;

import Service.LicentaException;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by User on 19-May-18.
 */



public abstract class AbstractServer {
    // atributele private ale clasei
    private int port;
    private ServerSocket server=null;
    protected abstract void processRequest(Socket client);

    // constructorul clasei
    public AbstractServer( int port){
        this.port=port;
    }

    // functia de start
    public void start() throws LicentaException {
        try{
            server=new ServerSocket(port);
            while(true){
                System.out.println("Waiting for clients ...");
                Socket client=server.accept();
                System.out.println("Client connected ...");
                processRequest(client);
            }
        }
        catch (IOException e) {
            throw new LicentaException("Starting server errror ",e);
        }
        finally {
            try{
                server.close();
            }
            catch (IOException e) {
                throw new LicentaException("Closing server error ", e);
            }
        }
    }

    // functia de stop
    public void stop() throws LicentaException {
        try {
            server.close();
        }
        catch (IOException e) {
            throw new LicentaException("Closing server error ", e);
        }
    }
}


