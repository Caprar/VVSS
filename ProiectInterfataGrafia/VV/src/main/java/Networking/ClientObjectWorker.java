package Networking;

import Domain.Carte;
import Service.IClient;
import Service.IServer;
import Service.LicentaException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 19-May-18.
 */



public class ClientObjectWorker implements  Runnable, IClient {
    // atributele private ale clasei
    private IServer server;
    private Socket connection;
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private volatile boolean connected;
    private List<Socket> connections=new ArrayList<Socket>();

    // constructorul clasei
    public ClientObjectWorker(IServer server, Socket connection) {
        this.server = server;
        this.connection = connection;
        connections.add(connection);

        try{
            output=new ObjectOutputStream(connection.getOutputStream());
            output.flush();
            input=new ObjectInputStream(connection.getInputStream());
            connected=true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // functia de run
    public void run() {
        while(connected){
            try {
                Object request = input.readObject();
                Object response = handleRequest((Request)request);

                if (response != null){
                    sendResponse((Response) response);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            input.close();
            output.close();
            connection.close();
        } catch (IOException e) {
            System.out.println("Error "+e);
        }
    }

    // functia de handle Request
    private Response handleRequest(Request request)  {
        Response response = null;

        // CARTE

        // actiunea care returneaza o entitatea
        // care are id dat
        if (request instanceof GetCarteRequest){
            System.out.println("Get (Id - Carte ) Request ");
            GetCarteRequest senReq = (GetCarteRequest)request;

            String idCarte = senReq.getIdCarte();

            Carte carte = new Carte();

            try {
                carte = server.getCarte(idCarte);
            } catch (LicentaException e) {
                e.printStackTrace();
            }

            return new GetCarteResponse(carte);
        }

        if (request instanceof GetAllRequest){
            System.out.println("Get (All ) Request ");
            GetAllRequest senReq = (GetAllRequest)request;

            List<Carte> carte = new ArrayList<>();

            try {
                carte = server.getAll();
            } catch (LicentaException e) {
                e.printStackTrace();
            }

            return new GetAllResponse(carte);
        }

        // actiunea de adaugare a unei entitati
        if (request instanceof AdaugareCarteRequest){
            if (request instanceof AdaugareCarteRequest){
                System.out.println("Adaugare ( Carte ) Request");
                AdaugareCarteRequest senReq = (AdaugareCarteRequest) request;

                Carte carte = senReq.getCarte();
                Carte carte1 = new Carte();

                try {
                    carte1 = server.getAdaugare(carte);
                } catch (LicentaException e) {
                    e.printStackTrace();
                }

                return new AdaugareCarteResponse(carte1);
            }
        }

        return response;
    }

    // functia de sendResponse
    private void sendResponse(Response response) throws IOException {
        System.out.println("sending response " + response);
        output.writeObject(response);
        output.flush();
    }
}
