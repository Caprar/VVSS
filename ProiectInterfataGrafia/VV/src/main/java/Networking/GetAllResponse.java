package Networking;

import Domain.Carte;

import java.util.List;

/**
 * Created by User on 19-May-18.
 */

public class GetAllResponse implements Response {
    private List<Carte> cartes;

    public GetAllResponse(List<Carte> cartes) {
        this.cartes = cartes;
    }

    public List<Carte> getCartes() {
        return this.cartes;
    }
}