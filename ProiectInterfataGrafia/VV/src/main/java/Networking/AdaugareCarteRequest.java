package Networking;

import Domain.Carte;

/**
 * Created by User on 19-May-18.
 */

public class AdaugareCarteRequest  implements Request {
    private Carte carte;

    public AdaugareCarteRequest(Carte carte) {
        this.carte = carte;
    }

    public Carte getCarte(){
        return this.carte;
    }
}
