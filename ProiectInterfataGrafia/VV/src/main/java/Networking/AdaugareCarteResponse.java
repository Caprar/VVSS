package Networking;

import Domain.Carte;

/**
 * Created by User on 19-May-18.
 */

public class AdaugareCarteResponse implements Response {
    private Carte carte;

    public AdaugareCarteResponse(Carte carte) {
        this.carte = carte;
    }

    public Carte getCarte(){
        return this.carte;
    }
}
