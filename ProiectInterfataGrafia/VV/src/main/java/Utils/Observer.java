package Utils;

/**
 * Created by User on 19-May-18.
 */

public interface Observer<E> {
    void update(Observable<E> observable);
}
