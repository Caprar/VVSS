package Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by User on 19-May-18.
 */


public class JdbcUtils {
    // atributele private ale clasei
    private Properties jdbcProps;
    private Connection instance=null;

    // constructorul clsei
    public JdbcUtils(Properties props){
        jdbcProps=props;
    }

    // functia de obtinere a unei noi conexiunii
    private Connection getNewConnection(){
        String driver=jdbcProps.getProperty("Licentaa.jdbc.driver");
        String url=jdbcProps.getProperty("Licentaa.jdbc.url");

        Connection con=null;
        try {
            Class.forName(driver);
            con= DriverManager.getConnection(url);
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading driver "+e);
        } catch (SQLException e) {
            System.out.println("Error getting connection "+e);
        }
        return con;
    }

    // functia de obtinere a conexiunii
    public Connection getConnection(){
        try {
            if (instance==null || instance.isClosed())
                instance=getNewConnection();
        } catch (SQLException e) {
            System.out.println("Error DB "+e);
        }
        return instance;
    }
}

