package Repository;

import java.util.List;

/**
 * Created by User on 19-May-18.
 */

public interface IRepository<ID,T> {
    int size();
    void save(T entity);
    void delete(ID id);
    void update(ID id);
    T findOne(ID id);
    List<T> findAll();
}
