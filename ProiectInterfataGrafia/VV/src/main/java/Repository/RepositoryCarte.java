package Repository;

import Domain.Carte;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by User on 19-May-18.
 */


public class RepositoryCarte implements IRepository<String, Carte>{
    // atributele private ale clasei
    private JdbcUtils dbutils;

    // constructor clasei pentru baza de date Abonare
    public RepositoryCarte(Properties props){
        dbutils=new JdbcUtils(props);
    }

    // functia care returneaza lungimea bazei de date Abonare
    @Override
    public int size() {
        Connection con = dbutils.getConnection();

        try(PreparedStatement preStmt = con.prepareStatement("select count(*) as [SIZE] from Carte")) {
            try(ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    return result.getInt("SIZE");
                }
            }
        } catch(SQLException ex){
            System.out.println("Error DB "+ex);
        }
        return 0;
    }

    // functia care adauga o entitate
    @Override
    public void save(Carte student) {
        Connection con = dbutils.getConnection();

        try(PreparedStatement preStmt = con.prepareStatement("insert into Carte values (?,?,?,?)")){
            preStmt.setString(1,student.getId());
            preStmt.setString(2,student.getReferenti());
            preStmt.setString(3,student.getAnAparitie());
            preStmt.setString(4,student.getCuvinteCheie());

            int result=preStmt.executeUpdate();
        } catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
    }

    // functia de stergere a unei etitati
    @Override
    public void delete(String id) {
        Connection con = dbutils.getConnection();

        try(PreparedStatement preStmt = con.prepareStatement("delete from Carte where Id=?")){
            preStmt.setString(1, id);
            int result = preStmt.executeUpdate();
        } catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
    }

    // functia de modificare a unei entitati
    // care are id-ul dat
    @Override
    public void update(String id) {
        Connection con = dbutils.getConnection();

        try(PreparedStatement preStmt = con.prepareStatement("update Carte set Autor=? where Id=?")){
            preStmt.setString(1,id);
            try(ResultSet result=preStmt.executeQuery()) {
                if (result.next()) {
                    String id1 = result.getString("Id");
                    String referenti = result.getString("Autor");
                    String an = result.getString("An");
                    String cuvinte = result.getString("Cuvinte");

                    Carte carte = new Carte(id, referenti, an, cuvinte);
                }
            }
        } catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
    }

    // functia de returnare a unei entitatii
    // care are id-ul dat
    @Override
    public Carte findOne(String id) {
        Connection con = dbutils.getConnection();

        try(PreparedStatement preStmt = con.prepareStatement("select * from Carte where id=?")){
            preStmt.setString(1,id);
            try(ResultSet result=preStmt.executeQuery()) {
                if (result.next()) {
                    String id1 = result.getString("Id");
                    String referenti = result.getString("Autor");
                    String an = result.getString("An");
                    String cuvinte = result.getString("Cuvinte");

                    Carte carte = new Carte(id, referenti, an, cuvinte);
                    return carte;
                }
            }
        } catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
        return null;
    }

    // functia de reurnare a unei entitatii
    // care are id-ul dat
    public Carte findCarte(String id) {
        Connection con = dbutils.getConnection();

        try(PreparedStatement preStmt = con.prepareStatement("select * from Carte where id=?")){
            preStmt.setString(1,id);
            try(ResultSet result=preStmt.executeQuery()) {
                if (result.next()) {
                    String id1 = result.getString("Id");
                    String referenti = result.getString("Autor");
                    String an = result.getString("An");
                    String cuvinte = result.getString("Cuvinte");

                    Carte carte = new Carte(id, referenti, an, cuvinte);
                    return carte;
                }
            }
        } catch (SQLException ex){
            System.out.println("Error DB "+ex);
        }
        return null;
    }

    // functia care returneaza o lista de entitatii
    @Override
    public List<Carte> findAll() {
        Connection con = dbutils.getConnection();
        List<Carte> students = new ArrayList<>();

        try(PreparedStatement preStmt = con.prepareStatement("select * from Carte")) {
            try(ResultSet result = preStmt.executeQuery()) {
                while (result.next()) {
                    String id1 = result.getString("Id");
                    String referenti = result.getString("Autor");
                    String an = result.getString("An");
                    String cuvinte = result.getString("Cuvinte");

                    Carte carte = new Carte(id1, referenti, an, cuvinte);
                    students.add(carte);
                }
            }
        } catch (SQLException e) {
            System.out.println("Error DB "+e);
        }
        return students;
    }
}


