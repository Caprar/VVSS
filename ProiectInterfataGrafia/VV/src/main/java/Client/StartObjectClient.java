package Client;

import Controller.ControllerClient;
import Networking.ServerObjectProxy;
import Service.IServer;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by User on 19-May-18.
 */



public class StartObjectClient {
    // atributele private ale clasei
    private static int defaultChatPort=1234;
    private static String defaultServer="localhost";
    private static IServer server;

    public static void Inits() {
        Properties clientProps=new Properties();
        try {
            clientProps.load(StartObjectClient.class.getResourceAsStream("/client.properties"));
            System.out.println("Client properties set. ");
            clientProps.list(System.out);
        } catch (IOException e) {
            System.err.println("Cannot find client.properties "+e);
            return;
        }

        String serverIP=clientProps.getProperty("Licentaa.server.host",defaultServer);
        int serverPort=defaultChatPort;

        try{
            serverPort=Integer.parseInt(clientProps.getProperty("Licentaa.server.port"));
        } catch(NumberFormatException ex){
            System.err.println("Wrong port number "+ex.getMessage());
            System.out.println("Using default port: "+defaultChatPort);
        }

        System.out.println("Using server IP "+serverIP);
        System.out.println("Using server port "+serverPort);

        server=new ServerObjectProxy(serverIP, serverPort);
        ControllerClient ctrl = new ControllerClient(server);
    }

    public static IServer getServer(){
        return server;
    }
}
