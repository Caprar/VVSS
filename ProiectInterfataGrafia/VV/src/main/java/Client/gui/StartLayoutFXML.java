package Client.gui;

import Client.Main;
import Controller.ControllerClient;
import Domain.Carte;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 19-May-18.
 */


public class StartLayoutFXML {
    // atributele private ale clasei
    Main mainApp;
    private ControllerClient controllerClient;
    @FXML
    private TableView<Carte> vizualizareTable;
    @FXML
    private TableColumn<Carte, String> titluColumn;
    @FXML
    private TableColumn<Carte, String> autorColumn;
    @FXML
    private TableColumn<Carte, String> anColumn;
    @FXML
    private TableColumn<Carte, String> edituraColumn;
    @FXML
    private TextField txtBoxTitlu;
    @FXML
    private TextField txtBoxAutor;
    @FXML
    private TextField txtBoxAn;
    @FXML
    private TextField txtBoxEditura;

    // constructorul clasei
    public void StartLayoutFXML(){}

    // functia pentru setarea aplicatiei
    public void setMainApplic(Main app) {
        this.mainApp=app;
    }

    // functia pentru setarea service-ului
    public void setService(ControllerClient controllerClient) {
        this.controllerClient = controllerClient;
        populare();
    }

    public void populare(){
       // populateTableVizualizare();
       // initializareTableVizualizare();
    }

    // functia de initializare a tabelului de date Student - Cadru didactic - Disciplina - Laborator - Enunt
    @FXML
    public void initializareTableVizualizare() {
        titluColumn.setCellValueFactory(new PropertyValueFactory<Carte, String>("Titlu"));
        autorColumn.setCellValueFactory(new PropertyValueFactory<Carte, String>("Autor"));
        anColumn.setCellValueFactory(new PropertyValueFactory<Carte, String>("An"));
        edituraColumn.setCellValueFactory(new PropertyValueFactory<Carte, String>("Editura"));
    }

    // functia de populare a tabelului de date Student - Cadru didactic - Disciplina - Laborator - Enunt
    public void populateTableVizualizare() {
        this.vizualizareTable.setItems(FXCollections.observableArrayList(this.controllerClient.getAll()));
    }


    // handle pentru adaugare
    @FXML
    public void handleAdaugare (ActionEvent actionEvent){
        String titlu = txtBoxTitlu.getText();
        String autor = txtBoxAutor.getText();
        String an = txtBoxAn.getText();
        String editura = txtBoxEditura.getText();

        Carte carte = new Carte(titlu, autor, an, editura);
        Carte carte1 = controllerClient.getAdaugare(carte);

        populateTableVizualizare();
        initializareTableVizualizare();

    }

}