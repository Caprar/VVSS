package Client;

import Client.gui.StartLayoutFXML;
import Controller.ControllerClient;
import Service.IServer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by User on 19-May-18.
 */



public class Main extends Application {
    // atributele private ale clasei
    BorderPane root = new BorderPane();
    AnchorPane center;
    ControllerClient serv;
    Stage primaryStage;
    private static StartObjectClient cl = new StartObjectClient();

    // functia de start
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("License");
        IServer s = cl.getServer();
        serv = new ControllerClient(s);
        initRootLayout();
        initStart();
    }

    // functia de main
    public static void main(String args[]) {
        cl.Inits();
        launch(args);
    }

    // afisarea ferestrei generale
    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/RootLayout.fxml"));
            root = loader.load();
            StartLayoutFXML rootController = loader.getController();
            rootController.setMainApplic(this);
            rootController.setService(serv);
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // afisarea ferestrei de Login
    public void initStart() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/startLayout.fxml"));
            center = (AnchorPane) loader.load();
            root.setCenter(center);
            StartLayoutFXML viewCtrl = loader.getController();
            viewCtrl.setMainApplic(this);
            viewCtrl.setService(serv);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


