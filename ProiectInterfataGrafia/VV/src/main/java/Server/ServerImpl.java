package Server;

import Domain.Carte;
import Repository.RepositoryCarte;
import Service.IClient;
import Service.IServer;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by User on 19-May-18.
 */

public class ServerImpl implements IServer {
    // atributele private ale clasei

    private RepositoryCarte repositoryCarte;

    private Map<String, IClient> loggesClients;


    // constructorul clasei
    public ServerImpl(RepositoryCarte repositoryCarte) {
        this.repositoryCarte = repositoryCarte;
        loggesClients = new ConcurrentHashMap<String, IClient>();
    }


    // functia de returnare a unei entitatii
    // care are id dat
    public Carte getCarte(String id) {
        return this.repositoryCarte.findCarte(id);
    }

    public Carte getAdaugare(Carte carte) {
        this.repositoryCarte.save(carte);
        return carte;
    }

    public List<Carte> getAll(){
        return this.repositoryCarte.findAll();
    }
}
