package Server;

import Networking.AbstractServer;
import Networking.ObjectConcurrentServer;
import Repository.RepositoryCarte;
import Service.IServer;
import Service.LicentaException;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by User on 19-May-18.
 */



public class StartObjectServer {
    // atributele private ale clasei
    private static int defaultPort=1234;

    // functia de main
    public static void main(String[] args) {
        Properties serverProps=new Properties();

        try {
            serverProps.load( StartObjectServer.class.getResourceAsStream("/server.properties"));
            System.out.println("Server properties set. ");
            serverProps.list(System.out);
        } catch (IOException e) {
            System.err.println("Cannot find server.properties "+e);
            return;
        }

        RepositoryCarte carteRepository = new RepositoryCarte(serverProps);

        IServer ServerImpl=new ServerImpl(carteRepository);

        int ServerPort=defaultPort;

        try {
            ServerPort = Integer.parseInt(serverProps.getProperty("Licentaa.server.port"));
        } catch (NumberFormatException nef){
            System.err.println("Wrong  Port Number"+nef.getMessage());
            System.err.println("Using default port "+defaultPort);
        }

        System.out.println("Starting server on port: "+ ServerPort);
        AbstractServer server = new ObjectConcurrentServer(ServerPort, ServerImpl);

        try {
            server.start();
        } catch (LicentaException e) {
            System.err.println("Error starting the server" + e.getMessage());
        }
    }
}


