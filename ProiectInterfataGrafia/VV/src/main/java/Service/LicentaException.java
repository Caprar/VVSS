package Service;

/**
 * Created by User on 19-May-18.
 */

public class LicentaException extends Exception{
    public LicentaException() {}

    public LicentaException(String message) {
        super(message);
    }

    public LicentaException(String message, Throwable cause) {
        super(message, cause);
    }
}
