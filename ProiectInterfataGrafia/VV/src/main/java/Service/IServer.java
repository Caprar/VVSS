package Service;

import Domain.Carte;

import java.util.List;

/**
 * Created by User on 19-May-18.
 */

public interface IServer {
    public Carte getCarte(String id) throws LicentaException;
    public Carte getAdaugare(Carte carte) throws LicentaException;
    public List<Carte> getAll() throws LicentaException;
}
