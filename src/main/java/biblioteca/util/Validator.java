package biblioteca.util;

import biblioteca.model.Carte;

import java.io.IOException;

public class Validator {
	
	public static boolean isStringOK(String s) throws IOException {
		boolean flag = s.matches("[a-zA-Z]+");
		if(flag == false)
			throw new IOException("String invalid");
		return flag;
	}
	
	public static void validateCarte(Carte carte)throws IOException{
		if(carte.getCuvinteCheie()==null){
			throw new IOException("Lista cuvinte cheie vida!");
		}
		if(carte.getReferenti()==null){
			throw new IOException("Lista autori vida!");
		}
		if(!isOKString(carte.getTitlu()))
			throw new IOException("Titlu invalid!");
		for(String s:carte.getReferenti()){
			if(!isOKString(s))
				throw new IOException("Autor invalid!");
		}
		for(String s:carte.getCuvinteCheie()){
			if(!isOKString(s))
				throw new IOException("Cuvant cheie invalid!");
		}
	}
	
	public static boolean isNumber(String s){
		return s.matches("[0-9]+");
	}
	
	public static boolean isOKString(String s){
		String []t = s.split(" ");
		if(t.length==2){
			boolean ok1 = t[0].matches("[a-zA-Z]+");
			boolean ok2 = t[1].matches("[a-zA-Z]+");
			if(ok1==ok2 && ok1==true){
				return true;
			}
			return false;
		}
		return s.matches("[a-zA-Z]+");
	}
}
