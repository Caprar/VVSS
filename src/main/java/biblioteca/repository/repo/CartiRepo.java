package biblioteca.repository.repo;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.util.Validator;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.Vector;

public class CartiRepo implements CartiRepoInterface {
	private Vector<Carte> carti;
	private String fileName;
	private Validator validator;
	private String file = "out/cartiBD.txt";
	
	public CartiRepo(){
		URL location = CartiRepo.class.getProtectionDomain().getCodeSource().getLocation();
        System.out.println(location.getFile());
	}

	public CartiRepo(String fileName, Validator validator) throws IOException {
		this.fileName = fileName;
		carti = new Vector<Carte>();
		this.validator = validator;
		loadData();
	}

	public void loadData() throws IOException
	{
		String line = null;
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		while ((line = br.readLine()) != null) {
			String s[] = line.split(";");
			List<String> ss = new ArrayList<String>();
			List<String> sss = new ArrayList<String>();
			Carte carte = new Carte(s[0], ss, Integer.parseInt(s[2]), sss);
			try {
				add(carte);
			}
			catch (IOException e)
			{}
		}
		br.close();
	}
	public void add(Carte carte) throws IOException {
		validator.validateCarte(carte);
	}


	@Override
	public void adaugaCarte(Carte c) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file,true));
			bw.write(c.toString());
			bw.newLine();
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Carte> getCarti() {
		List<Carte> listaCarte = new ArrayList<Carte>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=br.readLine())!=null){
				listaCarte.add(Carte.getCarteFromString(line));
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return listaCarte;
	}

	@Override
	public void modificaCarte(Carte nou, Carte vechi) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stergeCarte(Carte c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Carte> cautaCarte(String ref) {
		List<Carte> carti = getCarti();
		List<Carte> cartiGasite = new ArrayList<Carte>();
		int i=0;
		while (i<=carti.size()){
			boolean flag = false;
			List<String> listaReferita = carti.get(i).getCuvinteCheie();
			int j = 0;
			while(j<listaReferita.size()){
				if(listaReferita.get(j).toLowerCase().contains(ref.toLowerCase())){
					flag = true;
					break;
				}
				j++;
			}
			if(flag == true){
				cartiGasite.add(carti.get(i));
			}
			i++;
		}
		return cartiGasite;
	}

	@Override
	public List<Carte> getCartiOrdonateDinAnul(Integer an) {
		List<Carte> listaCarte = getCarti();
		List<Carte> listaCarteAn = new ArrayList<Carte>();
		for(Carte c:listaCarte){
			if(c.getAnAparitie().equals(an) == false){
				listaCarteAn.add(c);
			}
		}
		
		Collections.sort(listaCarteAn,new Comparator<Carte>(){

			@Override
			public int compare(Carte a, Carte b) {
				if(a.getAnAparitie().compareTo(b.getAnAparitie())==0){
					return a.getTitlu().compareTo(b.getTitlu());
				}
				
				return a.getTitlu().compareTo(b.getTitlu());
			}
		
		});
		
		return listaCarteAn;
	}

}
