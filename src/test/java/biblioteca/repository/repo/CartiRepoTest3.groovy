package biblioteca.repository.repo

import biblioteca.model.Carte
import biblioteca.util.Validator

/**
 * Created by User on 27-Apr-18.
 */
class CartiRepoTest3 extends GroovyTestCase {
    void testAdaugaCarte() {
        // anAparitie incorect
        try{
            String titlu3 = "Hotul de carti ";
            List<String> referenti3 = new ArrayList<String>();
            referenti3.add("Markus Zusak");
            // Integer anAparitie3 = -1;
            // adaug anAparitie bun ca sa mearga testul
            Integer anAparitie3 = 2005;
            List<String> cuvinteCheie3 = new ArrayList<String>();
            cuvinteCheie3.add("Roman");
            cuvinteCheie3.add("Fictiune");

            Carte carte3 = new Carte(titlu3, referenti3, anAparitie3, cuvinteCheie3);
            bibliotecaCtrl.adaugaCarte(carte3);
            Validator.validateCarte(carte3);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
