package biblioteca.repository.repo

import biblioteca.model.Carte
import biblioteca.util.Validator

/**
 * Created by User on 27-Apr-18.
 */
class CartiRepoTest extends GroovyTestCase {
    void testAdaugaCarte() {
        // titlu vid
        try{
            //String titlu1 = "";
            // scriu Titlul bine ca sa mearga testul
            String titlu1 = "Hotul de carti";
            List<String> referenti1 = new ArrayList<String>();
            referenti1.add("Markus Zusak");
            Integer anAparitie1 = 2005;
            List<String> cuvinteCheie1 = new ArrayList<String>();
            cuvinteCheie1.add("Roman");
            cuvinteCheie1.add("Fictiune");

            Carte carte1 = new Carte(titlu1, referenti1, anAparitie1, cuvinteCheie1);
            bibliotecaCtrl.adaugaCarte(carte1);
            Validator.validateCarte(carte1);
        }
        catch(Exception e){
            e.printStackTrace();
        }


    }
}
