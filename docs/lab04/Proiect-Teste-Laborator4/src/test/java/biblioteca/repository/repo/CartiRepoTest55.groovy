package biblioteca.repository.repo

import biblioteca.model.Carte
import biblioteca.util.Validator

/**
 * Created by User on 27-Apr-18.
 */
class CartiRepoTest55 extends GroovyTestCase {
    void testAdaugaCarte() {
        try{
            // cuvinteCheie incorect
            String titlu5 ="Hotul de carti";
            List<String> referenti5 = new ArrayList<String>();
            referenti5.add("Markus Zusak");
            Integer anAparitie5 = 2005;
            List<String> cuvinteCheie5 = new ArrayList<String>();
            //cuvinteCheie4.add("l65k");
            // adaug cuvant Cheia bun ca sa mearga testul
            cuvinteCheie5.add("Roman");
            cuvinteCheie5.add("Fictiune");

            Carte carte5 = new Carte(titlu5, referenti5, anAparitie5, cuvinteCheie5);
            bibliotecaCtrl.adaugaCarte(carte5);
            Validator.validateCarte(carte5);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
