package biblioteca.repository.repo;

import biblioteca.model.Carte;
import groovy.util.GroovyTestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 01-May-18.
 */

class RepositoryTest3 extends GroovyTestCase {
    private CartiRepo repo;

    @Before
    public void setUp(){
        repo = new CartiRepo();
    }

    // caz de testare – testare unitară pentru modulul A;
    @Test
    public void testareA(){
        int counter = repo.getCarti().size();
        String titlu = "Hotul de carti";
        List<String> referenti = new ArrayList<String>();
        referenti.add("Markus Zusak");
        Integer anAparitie = 2005;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);
        repo.adaugaCarte(carte);
        assertEquals(counter+1, repo.getCarti().size());
    }

    // caz de testare - testare de integrare a modulului B;
    @Test
    public void testareB(){
        // adaugam o carte, ca sa o cautam exact pe aceasta si sa stim exact atributele ei
        String titlu = "Hotul de carti";
        List<String> referenti = new ArrayList<String>();
        referenti.add("Markus Zusak");
        Integer anAparitie = 2005;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);

        repo.adaugaCarte(carte);

        List<Carte> cartiGasite = new ArrayList<Carte>();
        int counter = 1;

        String autor = "Mark";
        cartiGasite = repo.cautaCarte(autor);

        assertEquals(counter, cartiGasite.size());
    }

    // caz de testare - 3 testare de integrare a modulului C;
    @Test
    public void testareC(){
        // initial vom aduga cateva carti
        // testare pentru unitatea A
        // adaugarea unei noi carti

        int counter1 = repo.getCarti().size();

        List<String> referenti1 = new ArrayList<String>();
        referenti1.add("Markus Zusak");
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte1 = new Carte("Hotul de carti", referenti1, 2005, cuvinteCheie);
        repo.adaugaCarte(carte1);

        Carte carte2 = new Carte("Mesagerul", referenti1, 2000, cuvinteCheie);
        repo.adaugaCarte(carte2);

        assertEquals(counter1 + 2, repo.getCarti().size());

        List<String> referenti2 = new ArrayList<String>();
        referenti1.add("Richard Wurmbrand");

        Carte carte3 = new Carte("Drumul spre culmii", referenti2, 1977, cuvinteCheie);
        repo.adaugaCarte(carte3);

        assertEquals(counter1 + 3, repo.getCarti().size());

        counter1 = repo.getCarti().size();
        Carte carte4 = new Carte("Daca zidurile ar putea vorbi", referenti2, 1972, cuvinteCheie);
        repo.adaugaCarte(carte4);

        assertEquals(counter1 + 1, repo.getCarti().size());

        // caz de testare - testare pentru unitatea B
        // cautarea cartiilor scrise de un anumit autor

        List<Carte> cartiGasite1 = new ArrayList<Carte>();
        int counter2 = 2;

        String autor1 = "Markus Z";
        cartiGasite1 = repo.cautaCarte(autor1);

        assertEquals(counter2, cartiGasite1.size());

        List<Carte> cartiGasite2 = new ArrayList<Carte>();

        String autor2 = "Richard";
        cartiGasite2 = repo.cautaCarte(autor2);

        assertEquals(counter2, cartiGasite2.size());

        // caz de testare - testare pentru unitatea C
        // cautarea cartiilor care au aparut intr-un anumit an, sortate dupa titlu si autor

        int an1 = 2000;
        List<Carte> cartiGasite3 = new ArrayList<Carte>();
        int counter3 = 1;

        cartiGasite3 = repo.getCartiOrdonateDinAnul(an1);

        assertEquals(counter3, cartiGasite3.size());

        int an2 = 1977;
        List<Carte> cartiGasite4 = new ArrayList<Carte>();

        cartiGasite4 = repo.getCartiOrdonateDinAnul(an2);

        // cel putin o carte gasita
        assertEquals(counter3, cartiGasite4.size());

        int an3 = 1972;
        List<Carte> cartiGasite5 = new ArrayList<Carte>();

        cartiGasite5 = repo.getCartiOrdonateDinAnul(an2);

        // cel putin o carte gasita
        assertEquals(counter3, cartiGasite5.size());

        int an4 = 1903;
        int counter4 = 0;
        List<Carte> cartiGasite6 = new ArrayList<Carte>();

        cartiGasite6 = repo.getCartiOrdonateDinAnul(an2);

        // cel putin o carte gasita
        assertEquals(counter4, cartiGasite6.size());
    }
}