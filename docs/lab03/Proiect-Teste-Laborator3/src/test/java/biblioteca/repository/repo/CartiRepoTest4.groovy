package biblioteca.repository.repo

import biblioteca.model.Carte
import biblioteca.util.Validator

/**
 * Created by User on 27-Apr-18.
 */
class CartiRepoTest4 extends GroovyTestCase {
    void testAdaugaCarte() {
        try {
            // cuvinteCheie incorect
            String titlu4 = "";
            List<String> referenti4 = new ArrayList<String>();
            referenti4.add("Markus Zusak");
            Integer anAparitie4 = 2005;
            List<String> cuvinteCheie4 = new ArrayList<String>();
            //cuvinteCheie4.add("df99999");
            // adaug cuvant Cheia bun ca sa mearga testul
            cuvinteCheie4.add("Roman");
            cuvinteCheie4.add("Fictiune");

            Carte carte4 = new Carte(titlu4, referenti4, anAparitie4, cuvinteCheie4);
            bibliotecaCtrl.adaugaCarte(carte4);
            Validator.validateCarte(carte4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
