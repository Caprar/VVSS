package biblioteca.repository.repo

import biblioteca.model.Carte
import biblioteca.util.Validator

/**
 * Created by User on 27-Apr-18.
 */
class CartiRepoTest2 extends GroovyTestCase {
    void testAdaugaCarte() {
        // referent incorect
        try{
            String titlu2 = "Hotul de carti";
            List<String> referenti2 = new ArrayList<String>();
            //referenti2.add("2");
            // scriu autorul bine ca sa mearga
            referenti2.add("Markus Zusak");
            Integer anAparitie2 = 2005;
            List<String> cuvinteCheie2 = new ArrayList<String>();
            cuvinteCheie2.add("Roman");
            cuvinteCheie2.add("Fictiune");

            Carte carte2 = new Carte(titlu2, referenti2, anAparitie2, cuvinteCheie2);
            bibliotecaCtrl.adaugaCarte(carte2);
            Validator.validateCarte(carte2);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
