package biblioteca.view;


import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.util.Validator;
import sun.invoke.util.VerifyAccess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Consola {

	private BufferedReader console;
	BibliotecaCtrl bibliotecaCtrl;
	
	public Consola(BibliotecaCtrl bibliotecaCtrl){
		this.bibliotecaCtrl=bibliotecaCtrl;
	}
	
	public void executa() throws IOException {
		
		console = new BufferedReader(new InputStreamReader(System.in));
		
		int optiune = -1;
		while(optiune!=0){
			
			switch(optiune){
				case 1:
					adauga();
					break;
				case 2:
					cautaCartiDupaAutor();
					break;
				case 3:
					afiseazaCartiOrdonateDinAnul();
					break;
				case 4:
					afiseazaToateCartile();
					break;
			}
		
			printMenu();
			String line;
			do{
				System.out.println("Introduceti un nr:");
				line=console.readLine();
			}while(!line.matches("[0-4]"));
			optiune=Integer.parseInt(line);
		}
	}
	
	public void printMenu(){
		System.out.println("\n\n\n");
		System.out.println("Evidenta cartilor dintr-o biblioteca");
		System.out.println("     1. Adaugarea unei noi carti");
		System.out.println("     2. Cautarea cartilor scrise de un anumit autor");
		System.out.println("     3. Afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori");
		System.out.println("     4. Afisarea toturor cartilor");
		System.out.println("     0. Exit");
	}
	
	public void adauga(){
		Carte carte = new Carte();
		try{
			System.out.println("\n\n\n");
			
			System.out.println("Titlu:");
			carte.setTitlu(console.readLine());
			
			String line;
			Integer integer;
			do{
				System.out.println("An aparitie:");
				line=console.readLine();
			}
			while(!line.matches("[1-9]+"));
			carte.setAnAparitie(Integer.parseInt(line));
			
			do{
				System.out.println("Nr. de referent:");
				line=console.readLine();
			}
			while(!line.matches("[1-9]+"));
			int nrReferenti= Integer.parseInt(line);
			for(int i=1;i<=nrReferenti;i++){
				System.out.println("Autor "+i+": ");
				carte.adaugaReferent(console.readLine());
			}
			
			do{
				System.out.println("Nr. de cuvinte cheie:");
				line=console.readLine();
			}while(!line.matches("[1-9]+"));
			int nrCuv= Integer.parseInt(line);
			for(int i=1;i<=nrCuv;i++){
				System.out.println("Cuvant "+i+": ");
				carte.adaugaCuvantCheie(console.readLine());
			}
			
			bibliotecaCtrl.adaugaCarte(carte);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare() throws Exception {
		// merge
		try{
			String titlu = "Hotul de carti";
			List<String> referenti = new ArrayList<String>();
			referenti.add("Markus Zusak");
			Integer anAparitie = 2005;
			List<String> cuvinteCheie = new ArrayList<String>();
			cuvinteCheie.add("Roman");
			cuvinteCheie.add("Fictiune");

			Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);
			bibliotecaCtrl.adaugaCarte(carte);
			Validator.validateCarte(carte);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare1() throws Exception {
		// titlu vid
		try{
			//String titlu1 = "";
			// scriu Titlul bine ca sa mearga testul
			String titlu1 = "Hotul de carti";
			List<String> referenti1 = new ArrayList<String>();
			referenti1.add("Markus Zusak");
			Integer anAparitie1 = 2005;
			List<String> cuvinteCheie1 = new ArrayList<String>();
			cuvinteCheie1.add("Roman");
			cuvinteCheie1.add("Fictiune");

			Carte carte1 = new Carte(titlu1, referenti1, anAparitie1, cuvinteCheie1);
			bibliotecaCtrl.adaugaCarte(carte1);
			Validator.validateCarte(carte1);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare2() throws Exception {
		// referent incorect
		try{
			String titlu2 = "Hotul de carti";
			List<String> referenti2 = new ArrayList<String>();
			//referenti2.add("2");
			// scriu autorul bine ca sa mearga
			referenti2.add("Markus Zusak");
			Integer anAparitie2 = 2005;
			List<String> cuvinteCheie2 = new ArrayList<String>();
			cuvinteCheie2.add("Roman");
			cuvinteCheie2.add("Fictiune");

			Carte carte2 = new Carte(titlu2, referenti2, anAparitie2, cuvinteCheie2);
			bibliotecaCtrl.adaugaCarte(carte2);
			Validator.validateCarte(carte2);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare3() throws Exception {
		// anAparitie incorect
		try{
			String titlu3 = "Hotul de carti ";
			List<String> referenti3 = new ArrayList<String>();
			referenti3.add("Markus Zusak");
			// Integer anAparitie3 = -1;
			// adaug anAparitie bun ca sa mearga testul
			Integer anAparitie3 = 2005;
			List<String> cuvinteCheie3 = new ArrayList<String>();
			cuvinteCheie3.add("Roman");
			cuvinteCheie3.add("Fictiune");

			Carte carte3 = new Carte(titlu3, referenti3, anAparitie3, cuvinteCheie3);
			bibliotecaCtrl.adaugaCarte(carte3);
			Validator.validateCarte(carte3);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare4() throws Exception {
		try{
			// cuvinteCheie incorect
			String titlu4 ="";
			List<String> referenti4 = new ArrayList<String>();
			referenti4.add("Markus Zusak");
			Integer anAparitie4 = 2005;
			List<String> cuvinteCheie4 = new ArrayList<String>();
			//cuvinteCheie4.add("df99999");
			// adaug cuvant Cheia bun ca sa mearga testul
			cuvinteCheie4.add("Roman");
			cuvinteCheie4.add("Fictiune");

			Carte carte4 = new Carte(titlu4, referenti4, anAparitie4, cuvinteCheie4);
			bibliotecaCtrl.adaugaCarte(carte4);
			Validator.validateCarte(carte4);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare5() throws Exception {
		try{
			// cuvinteCheie incorect
			String titlu5 ="Hotul de carti";
			List<String> referenti5 = new ArrayList<String>();
			referenti5.add("Markus Zusak");
			Integer anAparitie5 = 2005;
			List<String> cuvinteCheie5 = new ArrayList<String>();
			//cuvinteCheie4.add("l65k");
			// adaug cuvant Cheia bun ca sa mearga testul
			cuvinteCheie5.add("Roman");
			cuvinteCheie5.add("Fictiune");

			Carte carte5 = new Carte(titlu5, referenti5, anAparitie5, cuvinteCheie5);
			bibliotecaCtrl.adaugaCarte(carte5);
			Validator.validateCarte(carte5);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare6() throws Exception {
		try{
			// an aparitie incorect
			String titlu6 ="Hotul de carti";
			List<String> referenti6 = new ArrayList<String>();
			referenti6.add("Markus Zusak");
			//Integer anAparitie6 = -2;
			// Adaug an aparitie bun ca sa mearga testul
			Integer anAparitie6 = 2005;
			List<String> cuvinteCheie6 = new ArrayList<String>();
			cuvinteCheie6.add("Roman");
			cuvinteCheie6.add("Fictiune");

			Carte carte6 = new Carte(titlu6, referenti6, anAparitie6, cuvinteCheie6);
			bibliotecaCtrl.adaugaCarte(carte6);
			Validator.validateCarte(carte6);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare7() throws Exception {
		try{
			// cuvinteCheie incorect
			String titlu7 ="Hotul de carti";
			List<String> referenti7 = new ArrayList<String>();
			referenti7.add("Markus Zusak");
			Integer anAparitie7 = 2005;
			List<String> cuvinteCheie7 = new ArrayList<String>();
			//cuvinteCheie4.add(9807);
			// adaug cuvant Cheia bun ca sa mearga testul
			cuvinteCheie7.add("Roman");
			cuvinteCheie7.add("Fictiune");

			Carte carte7 = new Carte(titlu7, referenti7, anAparitie7, cuvinteCheie7);
			bibliotecaCtrl.adaugaCarte(carte7);
			Validator.validateCarte(carte7);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare8() throws Exception {
		try{
			// titlu incorect
			//String titlu8 ="-1";
			// adaug titlul corect ca sa mearga testul
			String titlu8 ="Hotul de carti";
			List<String> referenti8 = new ArrayList<String>();
			referenti8.add("Markus Zusak");
			Integer anAparitie8 = 2005;
			List<String> cuvinteCheie8 = new ArrayList<String>();
			cuvinteCheie8.add("Roman");
			cuvinteCheie8.add("Fictiune");

			Carte carte8 = new Carte(titlu8, referenti8, anAparitie8, cuvinteCheie8);
			bibliotecaCtrl.adaugaCarte(carte8);
			Validator.validateCarte(carte8);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare9() throws Exception {
		try{
			// referenti incorect
			String titlu9 ="Hotul de carti";
			List<String> referenti9 = new ArrayList<String>();
			//referenti9.add("Mae9");
			// adug referent corect ca sa mearga testul
			referenti9.add("Markus Zusak");
			Integer anAparitie9 = 2005;
			List<String> cuvinteCheie9 = new ArrayList<String>();
			cuvinteCheie9.add("Roman");
			cuvinteCheie9.add("Fictiune");

			Carte carte9 = new Carte(titlu9, referenti9, anAparitie9, cuvinteCheie9);
			bibliotecaCtrl.adaugaCarte(carte9);
			Validator.validateCarte(carte9);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testAdaugare10() throws Exception {
		try{
			// cuvinteCheie incorect
			String titlu10 ="Hotul de carti";
			List<String> referenti10 = new ArrayList<String>();
			referenti10.add("Markus Zusak");
			Integer anAparitie10 = 2005;
			List<String> cuvinteCheie10 = new ArrayList<String>();
			//cuvinteCheie4.add(66-2);
			// adaug cuvant Cheia bun ca sa mearga
			cuvinteCheie10.add("Roman");
			cuvinteCheie10.add("Fictiune");

			Carte carte10 = new Carte(titlu10, referenti10, anAparitie10, cuvinteCheie10);
			bibliotecaCtrl.adaugaCarte(carte10);
			Validator.validateCarte(carte10);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void afiseazaToateCartile(){
		System.out.println("\n\n\n");
		try {
			for(Carte carte:bibliotecaCtrl.getCarti())
				System.out.println(carte);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cautaCartiDupaAutor(){
	
		System.out.println("\n\n\n");
		System.out.println("Autor:");
		try {
			for(Carte carte :bibliotecaCtrl.cautaCarte(console.readLine())){
				System.out.println(carte);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void afiseazaCartiOrdonateDinAnul(){
		System.out.println("\n\n\n");
		try{
			String line;
			do{
				System.out.println("An aparitie:");
				line=console.readLine();
			}while(!line.matches("[10-9]+"));
			for(Carte carte:bibliotecaCtrl.getCartiOrdonateDinAnul(line)){
				System.out.println(carte);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
