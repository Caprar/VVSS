package biblioteca.repository.repo;

import biblioteca.model.Carte;
import biblioteca.util.Validator;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by User on 27-Apr-18.
 */

public class RepositoryTest {
    private CartiRepo repo;

    @Before
    public void setUp(){
        repo = new CartiRepo();
    }

    @Test
    public void add_validCarte(){
        int counter = repo.getCarti().size();
        String titlu = "Hotul de carti";
        List<String> referenti = new ArrayList<String>();
        referenti.add("Markus Zusak");
        Integer anAparitie = 2005;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);
        repo.adaugaCarte(carte);
        assertEquals(counter+1, repo.getCarti().size());
    }

    @Test
    public void add_invalidCarte1()  {
        int counter = repo.getCarti().size();
        String titlu = "";
        List<String> referenti = new ArrayList<String>();
        referenti.add("Markus Zusak");
        Integer anAparitie = 2005;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);
        repo.adaugaCarte(carte);
        assertEquals(counter, repo.getCarti().size());
    }

    @Test
    public void add_invalidCarte2()  {
        int counter = repo.getCarti().size();
        String titlu = "Hotul de carti";
        List<String> referenti = new ArrayList<String>();
        referenti.add("");
        Integer anAparitie = 2005;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);
        repo.adaugaCarte(carte);
        assertEquals(counter, repo.getCarti().size());
    }

    @Test
    public void add_invalidCarte3()  {
        int counter = repo.getCarti().size();
        String titlu = "Hotul de carti ";
        List<String> referenti = new ArrayList<String>();
        referenti.add("Markus Zusak");
        Integer anAparitie = -1;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);
        repo.adaugaCarte(carte);
        assertEquals(counter, repo.getCarti().size());
    }

    @Test
    public void add_invalidCarte4()  {
        int counter = repo.getCarti().size();
        String titlu ="";
        List<String> referenti = new ArrayList<String>();
        referenti.add("Markus Zusak");
        Integer anAparitie = 2005;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);
        repo.adaugaCarte(carte);
        assertEquals(counter, repo.getCarti().size());
    }

    @Ignore
    @Test
    public void add_invalidQuiz1()  {

    }

    @After
    public void tearDown(){
        repo = null;
    }

    @Test
    public void cautareCarte1(){
        // adaugam o carte, ca sa o cautam exact pe aceasta si sa stim exact atributele ei
        String titlu = "Hotul de carti";
        List<String> referenti = new ArrayList<String>();
        referenti.add("Markus Zusak");
        Integer anAparitie = 2005;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);

        repo.adaugaCarte(carte);

        List<Carte> cartiGasite = new ArrayList<Carte>();
        int counter = 1;

        String autor = "Mark";
        cartiGasite = repo.cautaCarte(autor);

        assertEquals(counter, cartiGasite.size());
    }

    @Test
    public void cautareCarte2(){
        // adaugam o carte, ca sa o cautam exact pe aceasta si sa stim exact atributele ei
        String titlu = "Hotul de carti";
        List<String> referenti = new ArrayList<String>();
        referenti.add("Markus Zusak");
        Integer anAparitie = 2005;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);

        repo.adaugaCarte(carte);

        List<Carte> cartiGasite = new ArrayList<Carte>();
        int counter = 1;

        String autor = "Markus Zusakkk";
        cartiGasite = repo.cautaCarte(autor);

        assertEquals(counter, cartiGasite.size());
    }

    @Test
    public void cautareCarte3(){
        // adaugam o carte, ca sa o cautam exact pe aceasta si sa stim exact atributele ei
        String titlu = "Hotul de carti";
        List<String> referenti = new ArrayList<String>();
        referenti.add("Markus Zusak");
        Integer anAparitie = 2005;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);

        repo.adaugaCarte(carte);

        List<Carte> cartiGasite = new ArrayList<Carte>();
        int counter = 1;

        String autor = "MARKUS ZUSAK";
        cartiGasite = repo.cautaCarte(autor);

        assertEquals(counter, cartiGasite.size());
    }

    @Test
    public void cautareCarte4(){
        // adaugam o carte, ca sa o cautam exact pe aceasta si sa stim exact atributele ei
        String titlu = "Hotul de carti";
        List<String> referenti = new ArrayList<String>();
        referenti.add("Markus Zusak");
        Integer anAparitie = 2005;
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Roman");
        cuvinteCheie.add("Fictiune");

        Carte carte = new Carte(titlu, referenti, anAparitie, cuvinteCheie);

        repo.adaugaCarte(carte);

        List<Carte> cartiGasite = new ArrayList<Carte>();
        int counter = 1;

        String autor = "Zusak";
        cartiGasite = repo.cautaCarte(autor);

        assertEquals(counter, cartiGasite.size());
    }
}
